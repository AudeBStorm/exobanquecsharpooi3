﻿using BonusCarWash.Models;
using System;

namespace BonusCarWash
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Voiture Titine = new Voiture("2 BKG 704");
            Carwash TouProp = new Carwash();
            TouProp.Traiter(Titine);

            Console.ReadLine();
        }
    }
}
