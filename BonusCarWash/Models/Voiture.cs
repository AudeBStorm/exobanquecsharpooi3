﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BonusCarWash.Models
{
    public class Voiture
    {
        public string Plaque { get; }
        public Voiture(string Plaque)
        {
            this.Plaque = Plaque;
        }
    }
}
