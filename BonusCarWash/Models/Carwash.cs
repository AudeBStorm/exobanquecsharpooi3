﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BonusCarWash.Models
{
    public class Carwash
    {
        VoitureDelegate Del;

        public Carwash()
        {
            //Del += Preparer;
            //Del += Laver;
            //Del += Secher;
            //Del += Finaliser;

            //2ème version avec les méthodes anonymes
            Del += delegate (Voiture V) { Console.WriteLine($"Préparation de la voiture : {V.Plaque}"); };
            Del += delegate (Voiture V) { Console.WriteLine($"Lavage de la voiture : {V.Plaque}"); };
            Del += delegate (Voiture V) { Console.WriteLine($"Séchage de la voiture : {V.Plaque}"); };
            Del += delegate (Voiture V) { Console.WriteLine($"Finalisation de la voiture : {V.Plaque}"); };

        }
        //private void Preparer(Voiture V)
        //{
        //    Console.WriteLine($"Préparation de la voiture : {V.Plaque}");
        //}
        //private void Laver(Voiture V)
        //{
        //    Console.WriteLine($"Lavage de la voiture : {V.Plaque}");
        //}
        //private void Secher(Voiture V)
        //{
        //    Console.WriteLine($"Séchage de la voiture : {V.Plaque}");
        //}
        //private void Finaliser(Voiture V)
        //{
        //    Console.WriteLine($"Finalisation de la voiture : {V.Plaque}");
        //}
        public void Traiter(Voiture V)
        {
            if(Del != null)
            {
                Del(V);
                //Del.Invoke(V);
            }

            //Del?.Invoke(V);
        }
    }
}
