﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo6_ClasseAbstraite.Models
{
    public abstract class Compte
    {
        public string Numero { get; set; }
        public double Solde { get; private set;}
        public Personne Titulaire { get; set; }

        public static double operator +(double Somme, Compte Compte)
        {
            return Somme + ((Compte.Solde > 0)? Compte.Solde : 0);
        }
        public void Depot(double Montant)
        {
            if (Montant < 0)
            {
                return;
                //error
            }

            Solde += Montant;
        }

        public virtual void Retrait(double Montant)
        {
            if (Montant < 0)
            {
                return;
                //error
            }
            Solde -= Montant;
        }

        public void Description()
        {
            Console.WriteLine($"Le compte numéro {Numero}, appartenant à {Titulaire.Prenom}, contient {Solde} euros");
        }

        protected abstract double CalculInteret();

        public void AppliquerInteret()
        {
            //Si c'est un compte épargne, il fera le calcul avec CalculInteret de la classe Epargne
            //Si c'est un compte courant, avec CalculInteret de la classe Courant
            Solde = Solde + CalculInteret();
            //Solde += CalculInteret();
        }
    }
}
