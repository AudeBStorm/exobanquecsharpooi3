﻿using Exo6_ClasseAbstraite.Models;
using System;

namespace Exo6_ClasseAbstraite
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ///// CREATION DE PERSONNES /////
            //Pour créer une personne
            //Personne personne1 = new Personne();
            //personne1.Nom = "Beurive";
            //personne1.Prenom = "Aude";
            //personne1.DateDeNaissance = new DateTime(1989, 10, 16);
            Console.WriteLine("Création des personnes");
            Personne personne1 = new Personne()
            {
                Nom = "Beurive",
                Prenom = "Aude",
                DateDeNaissance = new DateTime(1989, 10, 16)
            };
            personne1.Presentation();

            Personne personne2 = new Personne()
            {
                Prenom = "Samuel",
                Nom = "Legrain",
                DateDeNaissance = new DateTime(1986, 09, 27)
            };
            personne2.Presentation();

            ///// CREATION DES COMPTES /////
            Console.WriteLine("Création des comptes");
            Courant compte1 = new Courant();
            compte1.Numero = "000001";
            compte1.Titulaire = personne1;
            compte1.LigneDeCredit = 200;
            compte1.Description();
            //compte1.Solde = 200; //Opération impossible car lecture seule !!

            compte1.Depot(100);
            compte1.Description();

            compte1.Retrait(150);
            compte1.Description();

            //Ce retrait ne fonctionnera pas puisqu'on sera sous la ligne de crédit
            compte1.Retrait(200);
            compte1.Description();

            Courant compte2 = new Courant()
            {
                Numero = "000002",
                LigneDeCredit = 100,
                Titulaire = personne2
            };
            compte2.Depot(3500);
            compte2.Description();

            Courant compte3 = new Courant()
            {
                Numero = "000003",
                LigneDeCredit = 150,
                Titulaire = personne1
            };
            compte3.Depot(500);
            Courant compte4 = new Courant()
            {
                Numero = "000004",
                LigneDeCredit = 150,
                Titulaire = personne1,
            };
            compte4.Depot(500);

            ///// CREATION D'UNE BANQUE /////
            //Banque MaSuperBanque = new Banque();
            //MaSuperBanque.Nom = "Ma super banque";
            Console.WriteLine();
            Console.WriteLine("Création d'une banque");
            Banque MaSuperBanque = new Banque()
            {
                Nom = "Ma super banque"
            };

            MaSuperBanque.AjouterCompte(compte1);
            MaSuperBanque["000001"].Description();

            MaSuperBanque.AjouterCompte(compte2);
            Console.WriteLine("Liste de tous les comptes :");
            MaSuperBanque.AfficherComptes();

            MaSuperBanque.SupprimerCompte("000001");
            Console.WriteLine("Liste de tous les comptes après suppression :");
            MaSuperBanque.AfficherComptes();


            MaSuperBanque.AjouterCompte(compte1);
            MaSuperBanque.AjouterCompte(compte3);
            MaSuperBanque.AjouterCompte(compte4);
            Console.WriteLine();
            Console.WriteLine("Les avoirs des comptes de Aude :");
            Console.WriteLine(MaSuperBanque.AvoirDesComptes(personne1));
            Console.WriteLine("Les avoirs des comptes de Sam :");
            Console.WriteLine(MaSuperBanque.AvoirDesComptes(personne2));


            Epargne Epargne1 = new Epargne()
            {
                Numero = "_____1",
                Titulaire = personne2
            };
            Epargne1.Depot(6500);
            Epargne1.Description();
            Epargne1.Retrait(500);
            Console.WriteLine(Epargne1.DateDeDernierRetrait.ToString());
            Epargne1.Description();

            //Ex5
            MaSuperBanque.AjouterCompte(Epargne1);
            Console.WriteLine("L'avoir des comptes de Sam : " + MaSuperBanque.AvoirDesComptes(personne2));

            //Ex6
            MaSuperBanque.AfficherComptes();
            Console.WriteLine();
            MaSuperBanque.InteretAnnuel();
            Console.WriteLine("Calcul des interêts en cours....");
            Console.WriteLine();
            MaSuperBanque.AfficherComptes();

            Console.ReadLine();
        }
    }
}
