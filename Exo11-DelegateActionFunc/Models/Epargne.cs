﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo10_Events.Models
{
    public class Epargne : Compte
    {
        
        public DateTime DateDeDernierRetrait { get; private set; }
        
        public Epargne(string Numero, Personne Titulaire) : base(Numero, Titulaire)
        {
            DateDeDernierRetrait = DateTime.Now;
        }

        public Epargne(string Numero, Personne Titulaire, double Solde) : base(Numero, Titulaire, Solde)
        {
            DateDeDernierRetrait = DateTime.Now;
        }

        public Epargne(string Numero, Personne Titulaire, double Solde, DateTime DateRetrait) : base(Numero, Titulaire, Solde)
        {
           
            DateDeDernierRetrait = DateRetrait;
        }
        public static double operator +(double Somme, Epargne Compte)
        {
            return Somme + ((Compte.Solde > 0) ? Compte.Solde : 0);
        }

        public override void Retrait(double Montant)
        {
            if(Solde - Montant < 0)
            {
                throw new SoldeInsuffisantException();
            }
            double AncienSolde = Solde;
            base.Retrait(Montant);
            //Solde -= Montant; //Plus besoin : action faite dans la méthode retrait du parent
            if (AncienSolde != Solde)
            {
                DateDeDernierRetrait = DateTime.Now;
            }
        }

        protected override double CalculInteret()
        {
            return Solde * (0.045);
        }
    }
}
