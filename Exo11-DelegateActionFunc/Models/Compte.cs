﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo11_DelegateActionFunc.Models
{
    public abstract class Compte : ICustomer, IBanker
    {
        //public event PassageEnNegatifDelegate PassageEnNegatifEvent = null;
        public Action<Compte> PassageEnNegatifEvent;
        public string Numero { get; private set; }
        public double Solde { get; private set;}
        public Personne Titulaire { get; private set; }
        
        public Compte(string Numero, Personne Titulaire) : this(Numero, Titulaire, 0)
        {
            
        }
        public Compte(string Numero, Personne Titulaire, double Solde)
        {
            this.Numero = Numero;
            this.Titulaire = Titulaire;
            this.Solde = Solde;
        }
        public static double operator +(double Somme, Compte Compte)
        {
            return Somme + ((Compte.Solde > 0)? Compte.Solde : 0);
        }
        public void Depot(double Montant)
        {
            if (Montant < 0)
            {
                throw new ArgumentOutOfRangeException("Le Montant à déposer ne peut pas être négatif");
            }

            Solde += Montant;
        }

        public virtual void Retrait(double Montant)
        {
            if (Montant < 0)
            {
                throw new ArgumentOutOfRangeException("Le Montant à retirer ne peut pas être négatif");

            }
            Solde -= Montant;
        }

        public void Description()
        {
            Console.WriteLine($"Le compte numéro {Numero}, appartenant à {Titulaire.Prenom}, contient {Solde} euros");
        }

        protected abstract double CalculInteret();

        public void AppliquerInteret()
        {
            //Si c'est un compte épargne, il fera le calcul avec CalculInteret de la classe Epargne
            //Si c'est un compte courant, avec CalculInteret de la classe Courant
            Solde = Solde + CalculInteret();
            //Solde += CalculInteret();
        }

        protected void DeclenchePassageEnNegatif()
        {
            if(PassageEnNegatifEvent != null)
            {
                PassageEnNegatifEvent.Invoke(this);
                //this : La méthode AlertPassageEnNegatif(Compte C) a besoin d'un compte en paramètre
                //Lorsqu'on déclenche l'évènement, on doit donc lui en fournir un
                //On doit lui fournir l'instance actuelle de notre Compte,
                //celui dans lequel il y a cet event en particulier donc this.
            }
            //PassageEnNegatifEvent?.Invoke(this);
            //Raccourcis : le ? signifie que la méthode Invoke() ne sera appelée que si
            //PassageEnNegatif n'est pas null
        }
    }
}
