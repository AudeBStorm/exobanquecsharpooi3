﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo10_Events.Models
{
    public class Banque
    {
        
        public string Nom { get; set; }

        Dictionary<string, Compte> Comptes = new Dictionary<string, Compte>();

        public Compte this[string NumeroCompte]
        {
            get {
                Compte compte;
                Comptes.TryGetValue(NumeroCompte, out compte);
                return compte;
            }
        }
        
        public void AjouterCompte(Compte Compte)
        {
            Comptes.Add(Compte.Numero, Compte);
            Compte.PassageEnNegatifEvent += AlertePassageEnNegatif;

        }

        public void SupprimerCompte(string NumeroCompte)
        {
            //On utilise l'indexeur crée dans la classe banque pour récupérer
            //le compte dont le numéro est celui passé en paramètre
            Compte CompteASupp = this[NumeroCompte];

            //.Find(predicat) => Pour chaque compte, si le numéro du compte correspond au numéro en paramètre,
            //on récupère le compte dans une variable CompteASupp
            //Compte CompteASupp = Comptes.Values.ToList().Find(p => p.Numero == NumeroCompte);
            if(CompteASupp != null)
            {
                CompteASupp.PassageEnNegatifEvent -= AlertePassageEnNegatif;
            }
            Comptes.Remove(NumeroCompte);
        }

        public void AfficherComptes()
        {
            foreach (Compte compte in Comptes.Values.ToArray())
            {
                compte.Description();
            }
        }

        //Méthode qui renvoie la somme de tous les comptes
        public double AvoirDesComptes(Personne Personne)
        {
            //Personne = personne1 => Aude
            double Avoirs = 0;
            foreach(Compte Compte in Comptes.Values.ToArray())
            {
                if(Compte.Titulaire == Personne)
                {
                   //Nous avons du rajouter la surchage d'opérateur dans la classe Compte 
                   //Pour que ceci fonctionne toujours
                    Avoirs = Avoirs + Compte;
                    
                }
            }
            return Avoirs;
        }
        
        public void InteretAnnuel()
        {
            //On applique l'interêt sur chacun des comptes
            //Si c'est un compte épargne, il fera le calcul avec CalculInteret de la classe Epargne
            //Si c'est un compte courant, avec CalculInteret de la classe Courant
            foreach(Compte Compte in Comptes.Values.ToArray())
            {
                Compte.AppliquerInteret();
            }
        }

        private void AlertePassageEnNegatif(Compte C)
        {
            Console.WriteLine($"Le compte {C.Numero} est passé en négatif");
        }
    }
}
