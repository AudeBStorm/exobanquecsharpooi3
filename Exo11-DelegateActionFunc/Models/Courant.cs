﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo11_DelegateActionFunc.Models
{
    public class Courant : Compte
    {
        
        private double _LigneDeCredit;
        
        public double LigneDeCredit
        {
            get { return _LigneDeCredit;}
            private set { 
                if(value < 0)
                {
                    throw new InvalidOperationException("La ligne de crédit ne peut être inférieure à 0");
                }
                _LigneDeCredit = value;
            }
        }

        public Courant(string Numero, Personne Titulaire) : base(Numero, Titulaire)
        {
            LigneDeCredit = 0;
        }

        public Courant(string Numero, Personne Titulaire, double Solde) : base(Numero, Titulaire, Solde)
        {
            LigneDeCredit = 0;
        }
       
        public Courant(string Numero, double LigneDeCredit, Personne Titulaire) : base(Numero, Titulaire)
        {
            this.LigneDeCredit = LigneDeCredit;
        }
        public static double operator +(double Somme, Courant Compte)
        {
            
            return Somme + ((Compte.Solde > 0) ? Compte.Solde : 0);

        }
        
        public override void Retrait(double Montant)
        {
            
            if(Solde - Montant < -LigneDeCredit)
            {
                throw new SoldeInsuffisantException();
            }
            
            if(Solde > 0 && Solde - Montant < 0)
            {
                //PassageEnNegatifEvent.Invoke(); 
                //Impossible d'invoquer un event provenant d'une autre classe, même parente
                DeclenchePassageEnNegatif(); 
                //On passera donc par une méthode protected dans le classe parent qui
                //elle, se charge de faire le Invoke()
            }

            base.Retrait(Montant);
        }

        protected override double CalculInteret()
        {
            //Si solde positif, on le mutiplie par 3%, sinon, par 9.75%
            //return Solde * ((Solde >= 0) ? 0.03 : 0.0975);

            if(Solde > 0)
            {
                return Solde * 0.03;
            }
            else
            {
                return Solde * 0.0975;
            }
        }
    }
}
