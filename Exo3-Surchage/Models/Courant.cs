﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo3_Surcharge.Models
{
    public class Courant
    {
        //////////////// PROPS //////////////////
        ///Propriétés privées
        private string _Numero;
        private double _Solde;
        private double _LigneDeCredit;
        private Personne _Titulaire;

        ///Propriétés publiques
        public string Numero
        {
            get { return _Numero; }
            set { _Numero = value; }
        }
        public double Solde
        {
            get { return _Solde; }
           // private set { if(value > 0) _Solde = value; }
        }
        public double LigneDeCredit
        {
            get { return _LigneDeCredit;}
            set { _LigneDeCredit = value;}
        }
        public Personne Titulaire
        {
            get { return _Titulaire; }
            set { _Titulaire = value;}
        }

        //////////// METHODES //////////
        ///Surchage de l'opérateur + afin qu'il renvoie la somme des soldes
        public static double operator +(double Somme, Courant Compte)
        {
            //Vérification du solde positif du compte
            //Avec une ternaire
            //(condition) ? "traitement si condition vraie" : "traitement si condition fausse"
            //(Est-ce que mon solde est supérieur à 0) ? Si oui j'ajoute le solde : Si non j'ajoute 0
            return Somme + ((Compte.Solde > 0) ? Compte.Solde : 0);


            //Avec un if-else
            //if(Compte.Solde > 0)
            //{
            //    return Somme + Compte.Solde;

            //}
            //else
            //{
            //    return Somme + 0;
            //}


        }
        public void Depot(double Montant)
        {
            //On vérifie que le montant est bien positif
            if(Montant < 0)
            {
                return;
                //Quand on aura appris comment, on renverra une erreur :) 
            }

            //2 écritures
            //_Solde = _Solde + Montant;
            _Solde += Montant;
        }

        public void Description()
        {
            Console.WriteLine($"Le compte numéro {Numero}, appartenant à {Titulaire.Prenom}, contient {Solde} euros");
        }

        public void Retrait(double Montant)
        {
            //On vérifie si le montant est positif
            if(Montant < 0)
            {
                return;
                //Quand on aura appris comment, on renverra une erreur :) 
            }
            
            if(_Solde - Montant < -LigneDeCredit)
            {
                return;
                //Quand on aura appris comment, on renverra une erreur :) 
            }

            //2 façons
            //_Solde = _Solde - Montant;
            _Solde -= Montant;

            //if (Montant > 0 && _Solde - Montant >= LigneDeCredit)
            //{
            //    //
            //}
            //else if (Montant < 0)
            //{

            //}else
            //{

            //}
        }
    }
}
