﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo3_Surcharge.Models
{
    public class Personne
    {
        //On crée les propriétés publiques en lecture et écriture
        public string Nom { get; set; }
        public string Prenom { get; set; }

        public DateTime DateDeNaissance { get; set; }

        //Ok aussi :
        //Création de toutes les variables privées _Nom, _Prenom, _DateDeNaissance
        //et de trois propriétés publiques avec get et set

        public void Presentation()
        {
            Console.WriteLine($"Création d'une personne, qui a pour nom {Nom} et prénom {Prenom} né.e le {DateDeNaissance.ToString()}");

        }
    }
}
