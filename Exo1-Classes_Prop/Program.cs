﻿using Exo1_Classes_Prop.Models;
using System;

namespace Exo1_Classes_Prop
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Pour créer une personne
            //Personne personne1 = new Personne();
            //personne1.Nom = "Beurive";
            //personne1.Prenom = "Aude";
            //personne1.DateDeNaissance = new DateTime(1989, 10, 16);

            Personne personne1 = new Personne()
            {
                Nom = "Beurive",
                Prenom = "Aude",
                DateDeNaissance = new DateTime(1989, 10, 16)
            };
            Console.WriteLine($"Création d'une personne, qui a pour nom {personne1.Nom} et prénom {personne1.Prenom} né.e le {personne1.DateDeNaissance.ToString()}");

            Courant compte1 = new Courant();
            compte1.Numero = "000001";
            compte1.Titulaire = personne1;
            compte1.LigneDeCredit = 200;
            Console.WriteLine($"Le compte numéro {compte1.Numero}, appartenant à {compte1.Titulaire.Prenom}, contient {compte1.Solde} euros");
            //compte1.Solde = 200; //Opération impossible car lecture seule !!

            compte1.Depot(100);
            Console.WriteLine($"Le compte numéro {compte1.Numero}, appartenant à {compte1.Titulaire.Prenom}, contient {compte1.Solde} euros");

            compte1.Retrait(150);
            Console.WriteLine($"Le compte numéro {compte1.Numero}, appartenant à {compte1.Titulaire.Prenom}, contient {compte1.Solde} euros");

            compte1.Retrait(200);
            Console.WriteLine($"Le compte numéro {compte1.Numero}, appartenant à {compte1.Titulaire.Prenom}, contient {compte1.Solde} euros");

            Console.ReadLine();
        }
    }
}
