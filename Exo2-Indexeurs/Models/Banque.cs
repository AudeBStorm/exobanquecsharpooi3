﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo2_Indexeurs.Models
{
    public class Banque
    {
        //Prop privées
        //Prop public get/set
        //Constructeur
        //Indexeurs
        //Methodes
        public string Nom { get; set; }

        Dictionary<string, Courant> Courants = new Dictionary<string, Courant>();

        public Courant this[string NumeroCompte]
        {
            get {
                Courant compte;
                Courants.TryGetValue(NumeroCompte, out compte);
                return compte;
            }
        }
        
        public void AjouterCompte(Courant CompteCourant)
        {
            Courants.Add(CompteCourant.Numero, CompteCourant);
        }

        public void SupprimerCompte(string NumeroCompte)
        {
            Courants.Remove(NumeroCompte);
        }

        public void AfficherComptes()
        {
            foreach (Courant compte in Courants.Values.ToArray())
            {
                compte.Description();
            }
        }

        
    }
}
