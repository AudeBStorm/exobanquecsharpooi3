﻿using Exo2_Indexeurs.Models;
using System;

namespace Exo2_Indexeurs
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ///// CREATION DE PERSONNES /////
            //Pour créer une personne
            //Personne personne1 = new Personne();
            //personne1.Nom = "Beurive";
            //personne1.Prenom = "Aude";
            //personne1.DateDeNaissance = new DateTime(1989, 10, 16);
            Console.WriteLine("Création des personnes");
            Personne personne1 = new Personne()
            {
                Nom = "Beurive",
                Prenom = "Aude",
                DateDeNaissance = new DateTime(1989, 10, 16)
            };
            personne1.Presentation();

            Personne personne2 = new Personne()
            {
                Prenom = "Samuel",
                Nom = "Legrain",
                DateDeNaissance = new DateTime(1986, 09, 27)
            };
            personne2.Presentation();

            ///// CREATION DES COMPTES /////
            Console.WriteLine("Création des comptes");
            Courant compte1 = new Courant();
            compte1.Numero = "000001";
            compte1.Titulaire = personne1;
            compte1.LigneDeCredit = 200;
            compte1.Description();
            //compte1.Solde = 200; //Opération impossible car lecture seule !!

            compte1.Depot(100);
            compte1.Description();

            compte1.Retrait(150);
            compte1.Description();

            //Ce retrait ne fonctionnera pas puisqu'on sera sous la ligne de crédit
            compte1.Retrait(200);
            compte1.Description();

            Courant compte2 = new Courant()
            {
                Numero = "000002",
                LigneDeCredit = 100,
                Titulaire = personne2
            };
            compte2.Depot(3500);
            compte2.Description();

            ///// CREATION D'UNE BANQUE /////
            //Banque MaSuperBanque = new Banque();
            //MaSuperBanque.Nom = "Ma super banque";
            Console.WriteLine();
            Console.WriteLine("Création d'une banque");
            Banque MaSuperBanque = new Banque()
            {
                Nom = "Ma super banque"
            };

            MaSuperBanque.AjouterCompte(compte1);
            MaSuperBanque["000001"].Description();

            MaSuperBanque.AjouterCompte(compte2);
            Console.WriteLine("Liste de tous les comptes :");
            MaSuperBanque.AfficherComptes();

            MaSuperBanque.SupprimerCompte("000001");
            Console.WriteLine("Liste de tous les comptes après suppression :");
            MaSuperBanque.AfficherComptes();






            Console.ReadLine();
        }
    }
}
