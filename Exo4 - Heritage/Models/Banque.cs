﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo4_Heritage.Models
{
    public class Banque
    {
        //Prop privées
        //Prop public get/set
        //Constructeur
        //Indexeurs
        //Methodes
        public string Nom { get; set; }

        Dictionary<string, Courant> Courants = new Dictionary<string, Courant>();

        public Courant this[string NumeroCompte]
        {
            get {
                Courant compte;
                Courants.TryGetValue(NumeroCompte, out compte);
                return compte;
            }
        }
        
        public void AjouterCompte(Courant CompteCourant)
        {
            Courants.Add(CompteCourant.Numero, CompteCourant);
        }

        public void SupprimerCompte(string NumeroCompte)
        {
            Courants.Remove(NumeroCompte);
        }

        public void AfficherComptes()
        {
            foreach (Courant compte in Courants.Values.ToArray())
            {
                compte.Description();
            }
        }

        //Méthode qui renvoie la somme de tous les comptes
        public double AvoirDesComptes(Personne Personne)
        {
            //Personne = personne1 => Aude
            double Avoirs = 0;
            foreach(Courant Compte in Courants.Values.ToArray())
            {
                //On parcourt la liste avec tous les comptes
                    //compte1 (dont le titulaire est Aude) //-50
                    //compte2 (dont le titulaire est Sam) //3500
                    //compte3 (dont le titulaire est Aude) //500
                    //compte4 (dont le titulaire est Aude) //500
                //Est-ce que le titulaire du compte actuellement parcouru
                //est la personne passée en paramètre ?
                if(Compte.Titulaire == Personne)
                {
                    //0 = 0 + (-50) => Comme négatif le -50 est remplacé par 0 => 0 + 0
                    //Avoir = 0
                    //0 = 0 + 500
                    //Avoir = 500
                    //500 = 500 + 500
                    //Avoir = 1000
                    Avoirs = Avoirs + Compte;
                    //Avoirs += Compte;
                }
            }
            return Avoirs;
        }
        
    }
}
