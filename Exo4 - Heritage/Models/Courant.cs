﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo4_Heritage.Models
{
    public class Courant : Compte
    {
        
        private double _LigneDeCredit;
        
        public double LigneDeCredit
        {
            get { return _LigneDeCredit;}
            set { _LigneDeCredit = value;}
        }
        
        public static double operator +(double Somme, Courant Compte)
        {
            
            return Somme + ((Compte.Solde > 0) ? Compte.Solde : 0);

        }
        
        public override void Retrait(double Montant)
        {
            
            if(Solde - Montant < -LigneDeCredit)
            {
                return;
                //Quand on aura appris comment, on renverra une erreur :) 
            }

            base.Retrait(Montant);
        }
    }
}
