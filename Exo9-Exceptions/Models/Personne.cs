﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo9_Exceptions.Models
{
    public class Personne
    {
        //On crée les propriétés publiques en lecture et écriture
        public string Nom { get; private set; }
        public string Prenom { get; private set; }

        public DateTime DateDeNaissance { get; private set; }

        public Personne(string Nom, string Prenom, DateTime DateDeNaissance)
        {
            this.Nom = Nom;
            this.Prenom = Prenom;
            this.DateDeNaissance = DateDeNaissance;
        }
        //Ok aussi :
        //Création de toutes les variables privées _Nom, _Prenom, _DateDeNaissance
        //et de trois propriétés publiques avec get et set

        public void Presentation()
        {
            Console.WriteLine($"Création d'une personne, qui a pour nom {Nom} et prénom {Prenom} né.e le {DateDeNaissance.ToString()}");

        }
    }
}
