﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo9_Exceptions.Models
{
    public abstract class Compte : ICustomer, IBanker
    {
        public string Numero { get; private set; }
        public double Solde { get; private set;}
        public Personne Titulaire { get; private set; }
        
        public Compte(string Numero, Personne Titulaire) : this(Numero, Titulaire, 0)
        {
            
        }
        public Compte(string Numero, Personne Titulaire, double Solde)
        {
            this.Numero = Numero;
            this.Titulaire = Titulaire;
            this.Solde = Solde;
        }
        public static double operator +(double Somme, Compte Compte)
        {
            return Somme + ((Compte.Solde > 0)? Compte.Solde : 0);
        }
        public void Depot(double Montant)
        {
            if (Montant < 0)
            {
                throw new ArgumentOutOfRangeException("Le Montant à déposer ne peut pas être négatif");
            }

            Solde += Montant;
        }

        public virtual void Retrait(double Montant)
        {
            if (Montant < 0)
            {
                throw new ArgumentOutOfRangeException("Le Montant à retirer ne peut pas être négatif");

            }
            Solde -= Montant;
        }

        public void Description()
        {
            Console.WriteLine($"Le compte numéro {Numero}, appartenant à {Titulaire.Prenom}, contient {Solde} euros");
        }

        protected abstract double CalculInteret();

        public void AppliquerInteret()
        {
            //Si c'est un compte épargne, il fera le calcul avec CalculInteret de la classe Epargne
            //Si c'est un compte courant, avec CalculInteret de la classe Courant
            Solde = Solde + CalculInteret();
            //Solde += CalculInteret();
        }
    }
}
