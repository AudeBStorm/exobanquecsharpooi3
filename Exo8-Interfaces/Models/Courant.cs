﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo7_Interfaces.Models
{
    public class Courant : Compte
    {
        
        private double _LigneDeCredit;
        
        public double LigneDeCredit
        {
            get { return _LigneDeCredit;}
            set { _LigneDeCredit = value;}
        }
        
        public static double operator +(double Somme, Courant Compte)
        {
            
            return Somme + ((Compte.Solde > 0) ? Compte.Solde : 0);

        }
        
        public override void Retrait(double Montant)
        {
            
            if(Solde - Montant < -LigneDeCredit)
            {
                return;
                //Quand on aura appris comment, on renverra une erreur :) 
            }

            base.Retrait(Montant);
        }

        protected override double CalculInteret()
        {
            //Si solde positif, on le mutiplie par 3%, sinon, par 9.75%
            //return Solde * ((Solde >= 0) ? 0.03 : 0.0975);

            if(Solde > 0)
            {
                return Solde * 0.03;
            }
            else
            {
                return Solde * 0.0975;
            }
        }
    }
}
