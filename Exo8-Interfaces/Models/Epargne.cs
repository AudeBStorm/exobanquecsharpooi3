﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo7_Interfaces.Models
{
    public class Epargne : Compte
    {
        
        public DateTime DateDeDernierRetrait { get; set; }
        
        public static double operator +(double Somme, Epargne Compte)
        {
            return Somme + ((Compte.Solde > 0) ? Compte.Solde : 0);
        }
        public override void Retrait(double Montant)
        {
            if(Solde - Montant < 0)
            {
                return; //error
            }
            double AncienSolde = Solde;
            base.Retrait(Montant);
            //Solde -= Montant; //Plus besoin : action faite dans la méthode retrait du parent
            if (AncienSolde != Solde)
            {
                DateDeDernierRetrait = DateTime.Now;
            }
        }

        protected override double CalculInteret()
        {
            return Solde * (0.045);
        }
    }
}
