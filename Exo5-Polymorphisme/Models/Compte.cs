﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo5_Polymorphisme.Models
{
    public class Compte
    {
        public string Numero { get; set; }
        public double Solde { get; private set;}
        public Personne Titulaire { get; set; }

        //On rajoute la surcharge d'operateur pour la classe Compte
        public static double operator +(double Somme, Compte Compte)
        {
            return Somme + ((Compte.Solde > 0)? Compte.Solde : 0);
        }
        public void Depot(double Montant)
        {
            if (Montant < 0)
            {
                return;
                //error
            }

            Solde += Montant;
        }

        public virtual void Retrait(double Montant)
        {
            if (Montant < 0)
            {
                return;
                //error
            }
            Solde -= Montant;
        }

        public void Description()
        {
            Console.WriteLine($"Le compte numéro {Numero}, appartenant à {Titulaire.Prenom}, contient {Solde} euros");
        }
    }
}
