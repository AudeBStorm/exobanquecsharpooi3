﻿using Exo10_Events.Models;
using System;

namespace Exo10_Events
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ///// CREATION DE PERSONNES /////
            //Pour créer une personne
            //Personne personne1 = new Personne();
            //personne1.Nom = "Beurive";
            //personne1.Prenom = "Aude";
            //personne1.DateDeNaissance = new DateTime(1989, 10, 16);
            Console.WriteLine("Création des personnes");
            Personne personne1 = new Personne("Beurive", "Aude", new DateTime(1989, 10, 16));
            Console.WriteLine(personne1.Nom);
            //personne1.Nom = "NouveauNom";
            //{
            //    Nom = "Beurive",
            //    Prenom = "Aude",
            //    DateDeNaissance = new DateTime(1989, 10, 16)
            //};
            personne1.Presentation();

            Personne personne2 = new Personne("Legrain", "Samuel", new DateTime(1986, 09, 27));
            //{
            //    Prenom = "Samuel",
            //    Nom = "Legrain",
            //    DateDeNaissance = new DateTime(1986, 09, 27)
            //};
            personne2.Presentation();

            ///// CREATION DES COMPTES /////
            Console.WriteLine("Création des comptes");
            //Courant compte1 = new Courant();
            //compte1.Numero = "000001";
            //compte1.Titulaire = personne1;
            //compte1.LigneDeCredit = 200;
            Courant compte1 = new Courant("000001", 200, personne1);
            compte1.Description();
            //compte1.Solde = 200; //Opération impossible car lecture seule !!


            compte1.Depot(100);
            compte1.Description();


            //compte1.Retrait(150);
            //compte1.Description();

            //Ce retrait ne fonctionnera pas puisqu'on sera sous la ligne de crédit
            compte1.Retrait(200);
            compte1.Description();

            Courant compte2 = new Courant("000002", 100, personne2);

            compte2.Depot(3500);
            compte2.Description();

            Courant compte3 = new Courant("000003", 150, personne1);

            compte3.Depot(500);
            Courant compte4 = new Courant("000004", 150, personne1);

            compte4.Depot(500);

            ///// CREATION D'UNE BANQUE /////
            //Banque MaSuperBanque = new Banque();
            //MaSuperBanque.Nom = "Ma super banque";
            Console.WriteLine();
            Console.WriteLine("Création d'une banque");
            Banque MaSuperBanque = new Banque()
            {
                Nom = "Ma super banque"
            };

            MaSuperBanque.AjouterCompte(compte1);
            MaSuperBanque["000001"].Description();

            MaSuperBanque.AjouterCompte(compte2);
            Console.WriteLine("Liste de tous les comptes :");
            MaSuperBanque.AfficherComptes();

            MaSuperBanque.SupprimerCompte("000001");
            Console.WriteLine("Liste de tous les comptes après suppression :");
            MaSuperBanque.AfficherComptes();


            MaSuperBanque.AjouterCompte(compte1);
            MaSuperBanque.AjouterCompte(compte3);
            MaSuperBanque.AjouterCompte(compte4);
            Console.WriteLine();
            Console.WriteLine("Les avoirs des comptes de Aude :");
            Console.WriteLine(MaSuperBanque.AvoirDesComptes(personne1));
            Console.WriteLine("Les avoirs des comptes de Sam :");
            Console.WriteLine(MaSuperBanque.AvoirDesComptes(personne2));


            Epargne Epargne1 = new Epargne("_____1", personne2);

            Epargne1.Depot(6500);
            Epargne1.Description();
            Epargne1.Retrait(500);
            Console.WriteLine(Epargne1.DateDeDernierRetrait.ToString());
            Epargne1.Description();

            //Ex5
            MaSuperBanque.AjouterCompte(Epargne1);
            Console.WriteLine("L'avoir des comptes de Sam : " + MaSuperBanque.AvoirDesComptes(personne2));

            //Ex6
            MaSuperBanque.AfficherComptes();
            Console.WriteLine();
            MaSuperBanque.InteretAnnuel();
            Console.WriteLine("Calcul des interêts en cours....");
            Console.WriteLine();
            MaSuperBanque.AfficherComptes();


            //Ex7
            //ICustomer Cu = new Courant();
            //ICustomer Cu = new Epargne();
            ICustomer Cu = MaSuperBanque["000003"];
            Console.WriteLine("Solde du compte customer : " + Cu.Solde);
            Cu.Depot(100);
            Cu.Retrait(50);
            //Cu.Titulaire.Nom //Impossible puisque c'est un ICustomer qui connait le Solde, Depot et Retrait
            Console.WriteLine("Solde du compte customer après dépôt et retrait : " + Cu.Solde);

            IBanker Ba = MaSuperBanque["000002"];
            Console.WriteLine("Solde du compte banker : " + Ba.Solde);
            Ba.Depot(2);
            Ba.Retrait(2350);
            Console.WriteLine("Solde du compte banker après dépôt et retrait : " + Ba.Solde);
            Console.WriteLine($"Le numéro du compte est {Ba.Numero} et le titulaire est {Ba.Titulaire.Prenom}");
            Ba.AppliquerInteret();
            Console.WriteLine("Solde du compte banker après interêt : " + Ba.Solde);

            //// EXERCICE 09 EXCEPTIONS ////
            try
            {
                compte1.Depot(500);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            compte1.Description();

            //Test dépassement de solde
            try
            {
                compte1.Retrait(6000);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (SoldeInsuffisantException ex)
            {
                Console.WriteLine(ex.Message);
            }

            //Test Montant négatif
            try
            {
                compte1.Retrait(-60);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (SoldeInsuffisantException ex)
            {
                Console.WriteLine(ex.Message);
            }

            try
            {
                Courant TestExcep = new Courant("Test", -500, personne1);
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
            }


            //Event
            //MaSuperBanque.AjouterCompte(compte1);
            //En ajoutant le compte dans notre banque,
            //on ajoute dans le delegate de l'event la méthode AlertePassageEnNegatif
            compte1.Retrait(400);

            Console.ReadLine();


        }
    }
}
