﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo10_Events.Models
{
    public class SoldeInsuffisantException : Exception
    {
        public SoldeInsuffisantException() : base("Opération impossible, le solde est insuffisant")
        {

        }
    }
}
