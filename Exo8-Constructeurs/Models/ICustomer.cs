﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo8_Constructeurs.Models
{
    public interface ICustomer
    {
        //Le niveau d'accessibilité ne peut être que public et c'est le niveau par défaut
        //il est donc optionnel
        //public double Solde { get; }
        double Solde { get; }

        void Depot(double Montant);
        void Retrait(double Montant);
    }
}
